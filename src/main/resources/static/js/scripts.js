function uploadPicture(formElement) {
    return $.ajax({
        type: "POST",
        url: "/home/files/upload",
        data: new FormData(formElement),
        dataType: 'json',
        contentType: false,
        cache: false,
        processData: false,
    })
}

function fillContestList(albums) {
    for (let i = 0; i < albums.length; i++) {
        console.log(albums[i])
        let listEl = $('<li class="list-group-item"></li>')
        listEl.val(albums[i]['id'])
        listEl.text(albums[i]['title'])
        $('#contestList').append(listEl[0])
        /*listEl.click(function (){
            redirect('/home/theFuckKnowsWhere')
        })*/
    }
}

function openInNewTab(url) {
    window.open(url, '_blank').focus();
}

function getPostById(postId) {
    return $.ajax({
        url: "/home/posts/get/" + postId,
        dataType: "json",
        type: "GET",
        contentType: "application/json"
    })
}

function getAlbumById(albumId) {
    return $.ajax({
        url: "/home/albums/get/" + albumId,
        dataType: "json",
        type: "GET",
        contentType: "application/json"
    })
}

function createPost(addPostJson) {
    return $.ajax({
        url: "/home/posts/create",
        type: "POST",
        dataType: "json",
        data: addPostJson,
        contentType: 'application/json',
    })
}
function addNewComment(form) {
    let data = formToJSON(form[0])
    return $.ajax({
        url: '/home/comments/create',
        data: data,
        dataType: "json",
        contentType: "application/json",
        type: "POST"
    })
}
function createRate(rateForm) {
    let data = formToJSON(rateForm[0])
    console.log(data)
    return $.ajax({
        url: '/home/rate/create',
        data: data,
        dataType: "json",
        contentType: "application/json",
        type: "POST"
    })
}

function createPostWithAlbum(addPostJson, albumId) {
    console.log(addPostJson)
    return $.ajax({
        url: "/home/posts/create/forAlbum/" + albumId,
        type: "POST",
        dataType: "json",
        data: addPostJson,
        contentType: 'application/json',
    })
}

function getAllAlbums() {
    return $.ajax({
        url: "/home/albums/get/all",
        type: "GET",
        dataType: "json"
    })
}

function updateUserProfilePicture(fileInfoJson) {
    let fileInfo = fileInfoJson[0]
    console.log(fileInfo)
    let updatedUserData = {
        'login': $('#login').val(),
        'firstName': $('#firstName').val(),
        'lastName': $('#lastName').val(),
        'email': $('#email').val(),
        'profilePicture': fileInfo
    }
    return updateUser(updatedUserData)
}

function getPostsByUser(pageNumber, userId) {
    return $.ajax({
        url: "/home/posts/byUsers/" + userId + "/page/" + pageNumber,
        type: "GET",
        dataType: "json"
    })
}

function updateUser(userObj) {
    return $.ajax({
        url: '/home/users/update',
        type: 'PUT',
        dataType: 'json',
        data: JSON.stringify(userObj),
        contentType: 'application/json',
        error: function (resp) {
            console.log(resp)
        }
    })
}

function updatePost(postDtoJson, postId) {
    return $.ajax({
        url: "/home/posts/update/" + postId,
        type: "PUT",
        dataType: "json",
        data: postDtoJson,
        contentType: "application/json",
        error: function (resp) {
            console.log(resp)
        }
    })
}

function signUp(jqForm) {
    let data = formToJSON(jqForm[0])
    console.log(data)
    return $.ajax({
        type: 'POST',
        url: '/signUp',
        data: data,
        dataType: "json",
        contentType: "application/json"
    })

}

function signIn(form) {
    let data = formToJSON(form[0])
    console.log(data)
    return $.ajax({
        type: 'POST',
        url: '/login',
        data: data,
        contentType: "application/json"
    })

}


function redirect(url) {
    location.href = url;
}

function getUser(userId) {
    let res = null
    $.ajax({
        url: '/home/users/' + userId,
        async: false,
        success: function (result) {
            res = result
        },
        error: function (response) {
            console.log('failed to get user by id: ' + userId)
            console.log(response)
        }
    })

    return res
}

function dynamicallyLoadPicture(elementId, imgSrc) {
    var _img = document.getElementById(elementId);
    var newImg = new Image;
    newImg.onload = function () {
        _img.src = this.src;
    }
    newImg.src = imgSrc;
}

function fillData(dto, JQelement = null) {
    if (JQelement == null) {
        for (let key in dto) {
            $('#' + key).val(dto[key])
        }
    } else {
        for (let key in dto) {
            JQelement.find('#' + key).val(dto[key])
        }
    }
}

function setCookie(name, value, seconds) {
    var expires = "";
    if (seconds) {
        var date = new Date();
        date.setTime(date.getTime() + (seconds * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function formToJSON(elem) {
    let output = {};
    new FormData(elem).forEach(
        (value, key) => {
            // Check if property already exist
            if (Object.prototype.hasOwnProperty.call(output, key)) {
                let current = output[key];
                if (!Array.isArray(current)) {
                    // If it's not an array, convert it to an array.
                    current = output[key] = [current];
                }
                current.push(value); // Add the new value to the array.
            } else {
                output[key] = value;
            }
        }
    );
    return JSON.stringify(output);
}

function disableButton(btn) {
    btn.attr('class', "btn btn-secondary btn-sm")
    btn.attr('disabled', true)
}

function enableButton(btn) {
    btn.attr('class', "btn btn-primary btn-sm")
    btn.attr('disabled', false)
}