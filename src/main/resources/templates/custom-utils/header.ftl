<#macro header_template>
    <header id="header" class="header">
        <div class="container">
            <div class="nav">
                <img src="<@spring.url '/img/aperture_logo.png'/>" alt="World Photo Awards Logo" class="logo">
                <ul class="menu">
                    <li>
                        <a href="/home/albums">
                            Contests
                        </a>
                    </li>
                    <li>
                        <a href="/home/profile">
                            Profile
                        </a>
                    </li>
                    <li>
                        <a href="/home/logout">
                            <#--todo logout-->
                            Log out
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
</#macro>