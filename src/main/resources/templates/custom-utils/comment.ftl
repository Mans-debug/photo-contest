<#macro comment_template comment>
    <div class="row">
        <div class="col-2">
            ${comment.user.login}
        </div>
        <div class="col-6">
            ${comment.text}
        </div>
    </div>
</#macro>