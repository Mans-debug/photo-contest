<#macro field elementId labelName value>
    <div class="form-group form-inline">
        <label for="${elementId}" class="col-sm-2 col-form-label">${labelName}</label>
        <div class="col-sm-10">
            <input type="text" readonly class="form-control-plaintext" id="${elementId}" value="${value}">
        </div>
    </div>
</#macro>