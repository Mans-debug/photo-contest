<#macro footer_template>
    <footer id="footer" class="footer">
        <div class="container">
            <div class="footer-text">
                <p>
                    World Photo Awards - live and inspire, 2022
                </p>
            </div>
        </div>
    </footer>
</#macro>