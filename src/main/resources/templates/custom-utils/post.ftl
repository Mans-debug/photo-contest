<#import "comment.ftl" as commentUtil/>
<#macro left_arrow>
    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
         class="bi bi-arrow-left-short" viewBox="0 0 16 16">
        <path fill-rule="evenodd"
              d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
    </svg>
</#macro>
<#macro right_arrow>
    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="currentColor"
         class="bi bi-arrow-right-short" viewBox="0 0 16 16">
        <path fill-rule="evenodd"
              d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
    </svg>
</#macro>
<#macro post_body id title description rating comments>
    <div class="card-body" style="font-size: 12px">
        <h5 class="card-title">${title}</h5>
        <p class="card-text">${description}</p>
        <a class="collapseBtn btn btn-secondary btn-sm" type="button">Comments</a>
        <br>
        <br>
        <div class="row">
            <div class="rateError"  role="alert" style="color: red">
            </div>
            <form class="sendRateForm col">
                <label> Rate the post:
                    <select name="value">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </label>
                <button class="sendRateBtn btn btn-outline-success btn-sm row col" type="button">Rate</button>
                <input class="idHolder" name="postId" type="hidden" value="${id}">
            </form>
            <div class="rateHolder col-5">
                Rating: ${rating}
            </div>
        </div>
        <div class="collapse" id="collapseExample">
            <div class="comments ">
                <#list comments as comment>
                    <@commentUtil.comment_template comment/>
                </#list>
            </div>
            <br>
            <br>
            <div class="commentError"  role="alert" style="color: red">
            </div>
            <form class="sendComment">
                <label>
                    Leave comment:
                    <input name="text">
                </label>
                <input class="idHolder" name="postId" type="hidden" value="${id}">
                <button type="button" class="btn btn-primary btn-sm">Send</button>
            </form>
        </div>
    </div>
</#macro>
<#macro post_template post>
    <div class="card">
        <!--arrows and picture-->
        <div class="row">
            <div class="col">
            </div>
            <div class="col-8">
                <#if post.photos??>
                    <#list post.photos as photo>
                        <img class="card-img-top" src="/home/files/download/${photo.id}" alt="photo">
                    </#list>
                </#if>
            </div>
            <div class="col" align="left">
            </div>
        </div>
        <@post_body post.id post.title post.description post.rating post.comments/>
    </div>
</#macro>
<#macro post_profile post>
    <div class="card">
        <!--arrows and picture-->
        <div class="row">
            <div class="col">
                <@left_arrow/>
            </div>
            <div class="col-8">
                <#if post.photos??>
                    <#list post.photos as photo>
                        <img class="card-img-top" src="/home/files/download/${photo.id}" alt="photo">
                    </#list>
                </#if>
            </div>
            <div class="col" align="left">
                <@right_arrow/>
            </div>
        </div>
        <@post_body_profile post.id post.title post.description post.rating post.comments/>
    </div>
</#macro>
<#macro post_body_profile id title description rating comments>
    <div class="card-body" style="font-size: 12px">
        <h5 class="card-title">${title}</h5>
        <p class="card-text">${description}</p>
        <div class="row">
            <div class="rateHolder col-5">
                Rating: ${rating}
            </div>
        </div>
        <br>
        <a class="collapseBtn btn btn-secondary btn-sm" type="button">Comments</a>
        <br>
        <br>
        <div class="collapse" id="collapseExample">
            <div class="comments ">
                <#list comments as comment>
                    <@commentUtil.comment_template comment/>
                </#list>
            </div>
            <br>
            <br>
            <div class="commentError"  role="alert" style="color: red">
            </div>
            <form class="sendComment">
                <label>
                    Leave comment:
                    <input name="text">
                </label>
                <input class="idHolder" name="postId" type="hidden" value="${id}">
                <button type="button" class="btn btn-primary btn-sm">Send</button>
            </form>
        </div>
    </div>
</#macro>