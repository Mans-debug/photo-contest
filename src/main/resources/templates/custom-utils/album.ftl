
<#macro album_title album>
    <li class="list-group-item" value="${album.id}">
        ${album.title}
    </li>
</#macro>

<#macro album_template album>
    <div class="card">
        <br>
        <h4 class="card-title">${album.title}</h4>
        <h6 class="card-body">${album.description}</h6>
        <a href="/home/posts/create/forAlbum/${album.id}" class="btn btn-outline-primary" style="margin-left: 240px" role="button" target="_blank"
           rel="noopener noreferrer">Add my post</a>
        <br>
    </div>
</#macro>
