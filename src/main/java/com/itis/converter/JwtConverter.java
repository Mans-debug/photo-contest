package com.itis.converter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.itis.models.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtConverter implements Converter<User, String> {

    @Value("${jwt.secret-key}")
    private String secretKey;
    @Value("${jwt.expiration-time}")
    private Integer expirationTime;

    @Override
    public String convert(User user) {
        return JWT.create()
                .withSubject(user.getId().toString())
                .withClaim("login", user.getLogin())
                .withClaim("role", user.getRole().name())
                .withExpiresAt(new Date(System.currentTimeMillis() + 1000 * 60 * expirationTime))
                .sign(Algorithm.HMAC256(secretKey));
    }

}
