package com.itis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication()
public class PhotoConstestRestApplication {
    public static void main(String[] args) {
        SpringApplication.run(PhotoConstestRestApplication.class, args);
    }

}
