package com.itis.validation.handlers;

import com.itis.exceptions.*;
import com.itis.validation.models.ValidationErrorDto;
import com.itis.validation.models.ValidationExceptionResponse;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ValidationExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<ValidationErrorDto> errors = new ArrayList<>();
        exception.getBindingResult().getAllErrors().forEach((error) -> {

            String errorName = error.getObjectName();
            String errorMessage = error.getDefaultMessage();
            String fieldName = null;
            BeanWrapper beanWrapper = new BeanWrapperImpl(error);

            if (beanWrapper.isReadableProperty("field")) {
                fieldName = (String) beanWrapper.getPropertyValue("field");

            }
            errors.add(ValidationErrorDto.builder()
                    .message(errorMessage)
                    .object(errorName)
                    .field(fieldName)
                    .build());
        });
        return ResponseEntity.status(status).body(ValidationExceptionResponse.builder()
                .errors(errors)
                .build());

    }

    @ExceptionHandler(AlbumNotFoundException.class)
    public String handleAlbumNotFound(Exception ex, WebRequest request, Model model) {
        model.addAttribute("error", fromException(ex));
        return "error/not_found";
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public String handleIllegalAccessException(Exception ex, WebRequest request, Model model) {
        model.addAttribute("error", fromException(ex));
        return "error/not_found";
    }

    @ExceptionHandler(NoEntityFoundException.class)
    public String handleNoEntityFoundException(Exception ex, WebRequest request, Model model) {
        model.addAttribute("error", fromException(ex));
        return "error/not_found";
    }

    @ExceptionHandler(NullFileException.class)
    public String handleNullFileException(Exception ex, WebRequest request, Model model) {
        model.addAttribute("error", fromException(ex));
        return "error/not_found";
    }

    @ExceptionHandler(PostNotFoundException.class)
    public String handlePostNotFoundException(Exception ex, WebRequest request, Model model) {
        model.addAttribute("error", fromException(ex));
        return "error/not_found";
    }

    @ExceptionHandler(UserNotFoundException.class)
    public String handleUserNotFoundException(Exception ex, WebRequest request, Model model) {
        model.addAttribute("error", fromException(ex));
        return "error/not_found";
    }

    private ValidationErrorDto fromException(Exception ex) {
        NoEntityFoundException exception = (NoEntityFoundException) ex;
        return ValidationErrorDto.builder().message(exception.getMessage()).object(exception.getObject().toString()).build();
    }
}
