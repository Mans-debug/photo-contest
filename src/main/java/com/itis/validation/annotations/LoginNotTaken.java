package com.itis.validation.annotations;

import com.itis.validation.validators.LoginNotTakenValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = LoginNotTakenValidator.class)
public @interface LoginNotTaken {
    String message() default "This login has been registered already";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
