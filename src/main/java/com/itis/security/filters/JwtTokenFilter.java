package com.itis.security.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.itis.repositories.UserRepository;
import com.itis.security.details.CustomUserDetails;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;


@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {

    public static final String AUTH_COOKIE = "Authorization";
    public static final String BEARER = "Bearer:";
    @Value("${jwt.secret-key}")
    private String secretKey;
    private final UserRepository userRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (request.getCookies() == null){
            filterChain.doFilter(request, response);
            return;
        }
        final Cookie cookie = Arrays
                .stream(request.getCookies())
                .filter(c -> c.getName().equals(AUTH_COOKIE))
                .findAny().orElse(null);
        if (cookie == null || !cookie.getValue().startsWith(BEARER)) {
            filterChain.doFilter(request, response);
            return;
        }
        String token = cookie.getValue().substring(BEARER.length());
        try {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC256(secretKey))
                    .build().verify(token);

            Long userId = Long.parseLong(decodedJWT.getSubject());
            UserDetails userDetails = new CustomUserDetails(userRepository.findById(userId).orElse(null));

            //todo check user null
            UsernamePasswordAuthenticationToken authentication =
                    new UsernamePasswordAuthenticationToken(userDetails,
                            decodedJWT.getSubject(),
                            userDetails.getAuthorities());

            authentication.setDetails(
                    new WebAuthenticationDetailsSource().buildDetails(request)
            );

            SecurityContextHolder.getContext().setAuthentication(authentication);
            filterChain.doFilter(request, response);
        } catch (JWTVerificationException e) {
            logger.warn("Login either has expired or wrong");
            filterChain.doFilter(request, response);
        }
    }
}
