package com.itis.security.oAuth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.itis.converter.JwtConverter;
import com.itis.exceptions.UserNotFoundException;
import com.itis.models.User;
import com.itis.repositories.UserRepository;
import com.squareup.okhttp.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
@RequiredArgsConstructor
@Slf4j
public class OAuth2ServiceImpl implements OAuth2Service {
    public final UserRepository userRepository;
    public final JwtConverter jwtConverter;
    private final OkHttpClient client = new OkHttpClient();

    @Value("${google.o-auth2.redirect-exchange}")
    private String redirectExchange;
    @Value("${google.people-api.batch-get}")
    private String batchGet;

    @Value("${google.o-auth2.client-id}")
    private String clientId;
    @Value("${google.o-auth2.client-secret}")
    private String clientSecret;

    @Value("${google.o-auth2.redirect-login.redirect-uri}")
    private String loginRedirectURI;


    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String authenticate(String code) throws IOException {
        String accessToken = getAccessToken(code);
        String email = getEmail(accessToken);
        User user = userRepository.findByEmail(email).orElseThrow(()->new UserNotFoundException(email, "Could not find user with email "));
        log.info("User %s has logged in via oAuth".formatted(user.getLogin()));
        return jwtConverter.convert(user);
    }

    private String getEmail(String accessToken) throws IOException {
        Request request = new Request.Builder()
                .url(batchGet.formatted("emailAddresses", "people/me", accessToken))
                .build();

        Call emailCall = client.newCall(request);
        Response response = emailCall.execute();
        ObjectNode node = objectMapper.readValue(response.body().string(), ObjectNode.class);
        return node.findValue("value").asText();
    }

    private String getAccessToken(String code) throws IOException {
        com.squareup.okhttp.RequestBody formBody = new FormEncodingBuilder()
                .add("code", code)
                .add("client_id", clientId)
                .add("client_secret", clientSecret)
                .add("redirect_uri", loginRedirectURI)
                .add("grant_type", "authorization_code")
                .build();
        Request request = new Request.Builder()
                .url(redirectExchange)
                .post(formBody)
                .build();

        Call tokenCall = client.newCall(request);
        Response response = tokenCall.execute();

        ObjectNode node = objectMapper.readValue(response.body().string(), ObjectNode.class);
        return node.findValue("access_token").asText();
    }
}
