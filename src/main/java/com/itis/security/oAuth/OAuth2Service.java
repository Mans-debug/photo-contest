package com.itis.security.oAuth;

import com.itis.models.User;

import java.io.IOException;

public interface OAuth2Service {
    String authenticate(String code) throws IOException;
}
