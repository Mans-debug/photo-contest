package com.itis.security.oAuth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@Slf4j
public class GoogleCodeEndPoint {
    private static final String AUTH_TOKEN_PREFIX = "Bearer:";
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    @Value("${google.o-auth2.redirect-login}")
    private String redirectLogin;
    @Value("${google.o-auth2.client-id}")
    private String clientId;
    @Value("${google.o-auth2.redirect-login.redirect-uri}")
    private String loginRedirectURI;
    @Value("${google.o-auth2.scopes}")
    private String scopes;

    @Autowired
    private OAuth2Service oAuth2Service;

    @GetMapping("/google/code")
    public void authTest(String code, HttpServletResponse response) throws IOException {
        String token = AUTH_TOKEN_PREFIX + oAuth2Service.authenticate(code);
        String url = "http://localhost/signIn?%s=%s".formatted(AUTHORIZATION_HEADER_NAME, token);
        response.setHeader("Location", url);
        response.setStatus(302);
    }

    @GetMapping(value = "/oAuth2Login")
    public void method(HttpServletResponse response) {
        String url = redirectLogin.formatted(clientId, loginRedirectURI, scopes, "code", "offline");
        response.setHeader("Location", url);
        response.setStatus(302);
    }
//    http://localhost/signIn?Authorization=Bearer:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxIiwicm9sZSI6IkFETUlOIiwibG9naW4iOiJsb2dpbjEyIiwiZXhwIjoxNjUzOTU2OTkxfQ.4TEzsbfrDYV5W5h6NigNmPbclGAoXqd-Q2l-Gd18lbg
}

