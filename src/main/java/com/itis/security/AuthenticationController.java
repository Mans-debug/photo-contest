package com.itis.security;

import com.itis.converter.JwtConverter;
import com.itis.dtos.DtoMapper;
import com.itis.dtos.SignInDto;
import com.itis.dtos.UserDto;
import com.itis.models.User;
import com.itis.security.details.CustomUserDetails;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final JwtConverter jwtConverter;


    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody @Valid SignInDto request) {
        try {
            Authentication authenticate = authenticationManager
                    .authenticate(
                            new UsernamePasswordAuthenticationToken(
                                    request.getLogin(), request.getPassword()
                            )
                    );
            CustomUserDetails customUserDetails = (CustomUserDetails) authenticate.getPrincipal();
            User user = customUserDetails.getUser();
            log.info("User %s has logged in".formatted(user.getLogin()));
            return ResponseEntity.ok()
                    .body(jwtConverter.convert(user));
        } catch (BadCredentialsException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

}