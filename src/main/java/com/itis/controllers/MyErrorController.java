package com.itis.controllers;

import com.itis.validation.models.ValidationErrorDto;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class MyErrorController implements ErrorController {
    private final String NOT_FOUND_PAGE = "error/not_found";
    private final String NOT_AUTHORIZED_PAGE = "error/forbidden";

    //todo 500_error page

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());

            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return NOT_FOUND_PAGE;
            } else if (statusCode == HttpStatus.FORBIDDEN.value()
                    || statusCode == HttpStatus.UNAUTHORIZED.value()) {
                return NOT_AUTHORIZED_PAGE;
            }
            /*else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                return "error-500";
            }*/
        }
        return NOT_FOUND_PAGE;
    }
}