package com.itis.controllers;

import com.itis.dtos.UserDto;
import com.itis.models.User;
import com.itis.security.details.CustomUserDetails;
import com.itis.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/home/users")
@RequiredArgsConstructor
public class UserController {
    public final UserService userService;

    @GetMapping("/{user-id}")
    public ResponseEntity<UserDto> getUser(@PathVariable("user-id") Long id){
        return ResponseEntity.ok(userService.get(id));
    }

    @PutMapping("/update")
    public ResponseEntity<UserDto> updateUser(@RequestBody @Valid UserDto userDto, Authentication authentication){
        Long userId = Long.valueOf((String) authentication.getCredentials());
        return ResponseEntity.ok(userService.update(userId,userDto));
    }
}
