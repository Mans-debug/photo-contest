package com.itis.controllers;

import com.itis.dtos.FileInfoDto;
import com.itis.services.FilesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/home/files")
@RequiredArgsConstructor
public class FileController {
    private final FilesService filesService;

    @PostMapping(value = "/upload")
    @ResponseBody
    public ResponseEntity<List<FileInfoDto>> uploadFiles(MultipartFile[] files){
       return ResponseEntity.ok(filesService.saveFiles(files));
    }

    @GetMapping(value = "/download/{id}")
    public void downloadFile(@PathVariable("id") Long id, HttpServletResponse response) throws IOException {
        filesService.downloadFile(id, response.getOutputStream());
        response.flushBuffer();
    }

}
