package com.itis.controllers;

import com.itis.dtos.AddCommentDto;
import com.itis.dtos.CommentDto;
import com.itis.services.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/home/comments")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService commentService;
    @PostMapping("/create")
    public CommentDto addComment(@RequestBody @Valid AddCommentDto addCommentDto,
                                                 Authentication authentication){
        Long userId = Long.valueOf((String) authentication.getCredentials());
        return commentService.addComment(userId, addCommentDto);
    }

    @GetMapping("/get/{id}")
    public CommentDto getComment(@PathVariable("id") Long commentId){
        return commentService.findById(commentId);
    }

}
