package com.itis.controllers;

import com.itis.services.PostService;
import com.itis.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/home/profile")
@RequiredArgsConstructor
public class ProfileController {
    private final UserService userService;
    private final PostService postService;
    @GetMapping
    public String getPage(Model model, Authentication authentication){
        Long userId = Long.valueOf((String) authentication.getCredentials());
        model.addAttribute("user", userService.get(userId));
        model.addAttribute("posts", postService.getPostsByUser(userId, 0).getPosts());
        return "home/profile";
    }

    @GetMapping("/best")
    public String getBestPosts(Model model, Authentication authentication){
        Long userId = Long.valueOf((String) authentication.getCredentials());

        model.addAttribute("user", userService.get(userId));
        model.addAttribute("posts", postService.getTopPostsByUser(userId, 0));
        return "home/profile";
    }
}
