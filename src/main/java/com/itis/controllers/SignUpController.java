package com.itis.controllers;

import com.itis.dtos.SignUpDto;
import com.itis.dtos.UserDto;
import com.itis.services.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/signUp")
@RequiredArgsConstructor
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getPage(Authentication authentication) {
        if(authentication != null)
            return "redirect:/";
        return "signUp";
    }

    @ResponseBody
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDto signUp(@RequestBody @Valid SignUpDto signUpDto) {
        return signUpService.signUp(signUpDto);
    }
}
