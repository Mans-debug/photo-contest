package com.itis.controllers;

import com.itis.dtos.AddRateDto;
import com.itis.dtos.RateDto;
import com.itis.services.RateService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/home/rate")
@RequiredArgsConstructor
public class RateController {
    private final RateService rateService;

    @PostMapping("/create")
    public void createRate(@RequestBody @Valid AddRateDto addRateDto,
                              Authentication authentication){
        Long userId = Long.valueOf((String) authentication.getCredentials());
       rateService.createRate(userId, addRateDto);
    }
}
