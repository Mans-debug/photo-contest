package com.itis.controllers;

import com.itis.dtos.AddPostDto;
import com.itis.dtos.PostDto;
import com.itis.dtos.PostsPage;
import com.itis.services.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/home/posts")
@RequiredArgsConstructor
public class PostController {
    private final PostService postService;
    private String pageNumber;

    @PostMapping("/create")
    public ResponseEntity<PostDto> createPost(@RequestBody @Valid AddPostDto postDto, Authentication authentication) {
        Long userId = Long.valueOf((String) authentication.getCredentials());
        return ResponseEntity.ok(postService.createPost(postDto, userId));
    }

    @GetMapping("/get/{post-id}")
    public ResponseEntity<PostDto> createPost(@PathVariable("post-id") Long postId) {
        return ResponseEntity.ok(postService.getById(postId));
    }

    @PutMapping("/update/{post-id}")
    public ResponseEntity<PostDto> updatePost(@RequestBody @Valid PostDto postDto,
                                              @PathVariable("post-id") Long postId,
                                              Authentication authentication) {
        Long userId = Long.valueOf((String) authentication.getCredentials());
        return ResponseEntity.ok(postService.update(postDto, postId, userId));
    }

    @DeleteMapping("/delete/{post-id}")
    public ResponseEntity<?> deletePost(@PathVariable("post-id") Long postId,
                                        Authentication authentication) {
        Long userId = Long.valueOf((String) authentication.getCredentials());
        postService.delete(postId, userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/byUsers/{user-id}/page/{page-num}")
    public ResponseEntity<PostsPage> getPosts(@PathVariable("user-id") Long userId,
                                              @PathVariable("page-num") Integer pageNumber) {
        return ResponseEntity.ok(postService.getPostsByUser(userId, pageNumber));
    }

    @GetMapping("/top/byUsers/{user-id}/page/{page-num}")
    public ResponseEntity<PostsPage> getBestPostsByUser(@PathVariable("user-id") Long userId,
                                                  @PathVariable("page-num") Integer pageNumber) {
        return ResponseEntity.ok(postService.getPostsByUser(userId, pageNumber));
    }

    @PostMapping("/create/forAlbum/{album-id}")
    public ResponseEntity<PostDto> createForAlbum(Authentication authentication,
                                                  @PathVariable("album-id") Long albumId,
                                                  @RequestBody @Valid AddPostDto addPostDto) {
        Long userId = Long.valueOf((String) authentication.getCredentials());
        return ResponseEntity.ok(postService.createForAlbum(addPostDto, albumId, userId));
    }

    @GetMapping("/create/forAlbum/{album-id}")
    public ModelAndView getPage(@PathVariable("album-id") Long albumId, ModelAndView model) {
        model.addObject("albumId", albumId);
        model.setViewName("home/addMyPost");
        return model;
    }

}
