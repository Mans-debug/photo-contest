package com.itis.controllers;

import com.itis.dtos.AddAlbumDto;
import com.itis.dtos.AlbumDto;
import com.itis.models.User;
import com.itis.repositories.AlbumRepository;
import com.itis.security.details.CustomUserDetails;
import com.itis.services.AlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/home/albums")
@RequiredArgsConstructor
public class AlbumController {
    private final AlbumService albumService;

    @PostMapping("/create")
    public ResponseEntity<AlbumDto> createAlbum(@RequestBody @Valid AddAlbumDto addAlbumDto) {
        return ResponseEntity.ok(albumService.createAlbum(addAlbumDto));
    }

    @GetMapping("/get/all")
    public ResponseEntity<List<AlbumDto>> getAllAlbums() {
        return ResponseEntity.ok(albumService.getAll());
    }

    @GetMapping("/get/{post-id}")
    public ResponseEntity<AlbumDto> getAlbumById(@PathVariable("post-id") Long albumId) {
        return ResponseEntity.ok(albumService.getById(albumId));
    }

    @GetMapping
    public ModelAndView getPage(ModelAndView model, Authentication authentication){
        var isAdmin= authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"));
        model.addObject("albums", albumService.getAll());
        model.addObject("isAdmin", isAdmin);
        model.setViewName("home/albums");
        return model;
    }

    @GetMapping("/{album-id}")
    public ModelAndView getPage(ModelAndView model, @PathVariable("album-id") Long albumId){
        model.addObject("album", albumService.getById(albumId));
        model.setViewName("home/album");
        return model;
    }
}
