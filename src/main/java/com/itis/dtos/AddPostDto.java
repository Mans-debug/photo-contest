package com.itis.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Модель для добавления поста определенному пользователю")
public class AddPostDto {

    @NotBlank(message = "mustn't be empty")
    @Size(min = 3, max = 32, message = "Title must contain 3 to 32 letters")
    @Schema(description = "Название поста", example = "Название поста")
    private String title;

    @Schema(description = "Описание поста", example = "Отдыхаю с подружками в Турции", nullable = true)
    @Size(max = 255, message = "Description is too long")
    private String description;

    @Schema(description = "Рейтинг поста", defaultValue = "0.0")
    private Double rating = 0.0;

    @Schema(description = "fileInfoDtos")
    private List<FileInfoDto> photos;
}
