package com.itis.dtos;

import com.itis.models.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DtoMapper {

    @Mapping(target = "role", expression = "java(User.Role.USER)")
    User signUpDtoToUser(SignUpDto signUpDto);

    UserDto userToUserDto(User user);

    @Mapping(source = "id", target = "id", ignore = true)
    User updateUser(@MappingTarget User user, UserDto userDto);

    @Mapping(source = "photos", target = "photos")
    Post addPostToPost(AddPostDto addPostDto);

    @Mapping(target = "rating", expression = "java(calculateAvgRating(post))")
    PostDto postToPostDto(Post post);

    @Mapping(source = "id", target = "id", ignore = true)
    Post updatePost(@MappingTarget Post post, PostDto postDto);

    FileInfoDto fileInfoToFileInfoDto(FileInfo fileInfo);

    List<PostDto> postsToPostsDto(List<Post> content);

    Album addAlbumDtoToAlbum(AddAlbumDto addAlbumDto);

    Album albumDtoToAlbum(AlbumDto albumDto);

    AlbumDto albumToAlbumDto(Album album);

    List<AlbumDto> albumListToAlbumDtoList(List<Album> albums);

    @Mapping(target = "timeCommented", expression = "java(LocalDateTime.now())")
    Comment addCommentDtoToComment(AddCommentDto addCommentDto);

    CommentDto commentToCommentDto(Comment comment);

    RateDto rateToRateDto(Rate rate);

    default Double calculateAvgRating(Post post){
        List<Rate> rates = post.getRate();
        if (rates == null || rates.size() == 0)
            return 0d;
        return rates.stream().mapToDouble(Rate::getValue).average().orElse(0d);
    }
}
