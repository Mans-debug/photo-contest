package com.itis.dtos;

import com.itis.validation.annotations.EmailNotTaken;
import com.itis.validation.annotations.LoginNotTaken;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Модель для регистрации пользователя")
@EmailNotTaken
@LoginNotTaken
public class SignUpDto {

    @NotBlank(message = "There must be no empty fields")
    @Size(min = 3, max = 32, message = "First name must contain 3 to 32 letters")
    @Pattern(regexp = "[A-Za-zА-Яа-я]+", message = "First name should consists only of letters")
    @Schema(description = "Имя", example = "Иван")
    private String firstName;

    @NotBlank(message = "There must be no empty fields")
    @Size(min = 3, max = 32, message = "Last name must contain 3 to 32 letters")
    @Pattern(regexp = "[A-Za-zА-Яа-я]+", message = "Last name should consists only of letters")
    @Schema(description = "Фамилия", example = "Иванов")
    private String lastName;

    @NotBlank(message = "There must be no empty fields")
    @Size(min = 3, message = "Password must contain at least 3 symbols")
    @Schema(description = "Пароль", example = "password")
    private String password;

    @NotBlank(message = "There must be no empty fields")
    @Email(message = "Incorrect email")
    @Schema(description = "Почта пользователя", example = "test@gmail.com")
    private String email;

    @NotBlank(message = "There must be no empty fields")
    @Size(min = 5, message = "Login must contain at least 5 symbols")
    @Schema(description = "Логин пользователя, для входа в аккаунт", example = "test1")
    private String login;

}
