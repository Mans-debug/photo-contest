package com.itis.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Модель для пользователя")
public class UserDto {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 32)
    @Pattern(regexp = "[A-Za-zА-Яа-я]+")
    @Schema(description = "Имя", example = "Иван")
    private String firstName;
    @NotBlank
    @Size(min = 3, max = 32)
    @Pattern(regexp = "[A-Za-zА-Яа-я]+")
    @Schema(description = "Фамилия", example = "Иванов")
    private String lastName;
    @NotBlank
    @Email
    @Schema(description = "Почта пользователя", example = "test@gmail.com")
    private String email;
    @NotBlank
    @Size(min = 5)
    @Schema(description = "Логин пользователя, для входа в аккаунт", example = "test1")
    private String login;
    @Valid
    @Schema(description = "Аватарка", nullable = true)
    private FileInfoDto profilePicture;
}
