package com.itis.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Модель для добавления нового альбома")
public class AddAlbumDto {

    @NotBlank(message = "Title mustn't be empty")
    @Size(min = 3, max = 32, message = "Title must contain 3 to 32 letters")
    @Schema(description = "Название альбома", example = "Название альбома")
    private String title;

    @Schema(description = "Описание альбома", example = "Описание альбома", nullable = true)
    @Size(max = 255, message = "Description is too long")
    private String description;

    @NotBlank(message = "Date mustn't be empty")
    private String startDate;

    @NotBlank(message = "Date mustn't be empty")
    private String endDate;
}
