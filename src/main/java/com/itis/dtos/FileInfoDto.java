package com.itis.dtos;

import com.itis.models.FileInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Модель для файла")
public class FileInfoDto {
    @Schema(description = "Название файла на сервере")
    private String storageName;
    @Schema(description = "ID файла")
    private Long id;

}
