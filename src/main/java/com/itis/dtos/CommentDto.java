package com.itis.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Schema(description = "Модель для комментария")
public class CommentDto {
    @Valid
    @Schema(description = "Пользователь оставивший комментарий")
    private UserDto user;
    @Schema(description = "Время, когда был оставлен комментарий", pattern = "yyyy:mm:dd HH:SS")
    private String timeCommented;
    @Schema(description = "Текст комментария", example = "Огонь!!!")
    @NotBlank
    private String text;
}
