package com.itis.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Модель для альбома")
public class AlbumDto {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 32)
    @Schema(description = "Название альбома", example = "Название альбома")
    private String title;

    @Schema(description = "Описание альбома", example = "Описание альбома", nullable = true)
    @Size(max = 255)
    private String description;

    @Valid
    @Schema(description = "Посты прикрепленные к альбому")
    private List<PostDto> posts;

    @NotBlank
    private String startDate;

    @NotBlank
    private String endDate;

}
