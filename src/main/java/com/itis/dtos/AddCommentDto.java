package com.itis.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddCommentDto {
    @NotBlank(message = "Comment mustn't be empty")
    private String text;
    @NotNull
    private Long postId;
}
