package com.itis.dtos;

import com.itis.models.Post;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Страница с постами")
public class PostsPage {
    @Schema(description = "посты")
    private List<PostDto> posts;
    @Schema(description = "Сколько всего страниц")
    private Integer totalPages;
}
