package com.itis.dtos;

import com.itis.models.FileInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Schema(description = "Модель поста")
public class PostDto {
    @NotNull
    @Min(value = 1)
    @Schema(description = "ID поста", example = "1")
    private Long id;

    @NotBlank
    @Size(min = 3, max = 32)
    @Schema(description = "Название поста", example = "Название поста")
    private String title;

    @Schema(description = "Описание поста", example = "Отдыхаю с подружками в Турции", nullable = true)
    @Size(max = 255)
    private String description;

    @Schema(description = "Рейтинг поста", defaultValue = "0.0")
//    @Builder.Default
    private Double rating = 0.0;

    @Valid
    @Schema(description = "Комментарии к посту", nullable = true)
    @NotNull
    private List<CommentDto> comments;

    @Valid
    @Schema(description = "Фотографии к посту")
    @NotNull
    List<FileInfoDto> photos;
}
