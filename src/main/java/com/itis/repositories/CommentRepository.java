package com.itis.repositories;


import com.itis.models.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
