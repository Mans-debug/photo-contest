package com.itis.repositories;

import com.itis.models.Comment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

@Repository
@RequiredArgsConstructor
public class CommentRepositoryImpl implements CommentRepository{
    private final EntityManager entityManager;


    @Override
    @Transactional
    public Comment save(Comment comment) {
        entityManager.persist(comment);
        return comment;
    }

    @Override
    public Comment findById(Long commentId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Comment> query = builder.createQuery(Comment.class);
        Root<Comment> root = query.from(Comment.class);

        Predicate ID = builder.equal(root.get("id"), commentId);
        query.where(builder.and(ID));
        return entityManager.createQuery(query.select(root)).getSingleResult();
    }

    @Override
    public Comment update(Comment comment) {
        return save(comment);
    }

    @Override
    public void deleteById(Long commentId) {
        Query query = entityManager.createQuery("delete from Comment c where c.id =" + commentId);
    }
}
