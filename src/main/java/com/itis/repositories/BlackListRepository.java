package com.itis.repositories;

public interface BlackListRepository {
    default void save(String token){}

    default boolean exists(String token){return false;}
}
