package com.itis.repositories;

public interface CrudRepository<E, K> {
    E save(E e);

    E findById(K k);

    E update(E e);

    void deleteById(K k);
}
