package com.itis.repositories;


import com.itis.models.Post;
import com.itis.models.User;
import io.swagger.v3.core.util.OptionalUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByLogin(String login);
    Optional<User> findByEmail(String email);
    Boolean existsByEmail(String email);
    Boolean existsByLogin(String login);

}
