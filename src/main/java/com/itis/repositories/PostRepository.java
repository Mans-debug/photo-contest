package com.itis.repositories;


import com.itis.models.Post;
import com.itis.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PostRepository extends JpaRepository<Post, Long> {

    Optional<Post> findByUserAndId(User user, Long postId);

    Page<Post> findAllByUser(User user, Pageable pageable);

    @Query("from Post p where p in" +
            " (select r from Rate r where r.value >= (select max(r.value) * 0.8 from Rate r))" +
            " and p.user = :user")
    List<Post> userTop(@Param("user") User user);

    List<Post> findByUser(User user);
}
