package com.itis.repositories;

import com.itis.models.Post;
import com.itis.models.Rate;
import com.itis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface RateRepository extends JpaRepository<Rate, Long> {

    Optional<Rate> findByUserAndPost(User user, Post post);

    @Query(value = "from Rate r where r.user.id = :user and r.post.id = :post")
    Optional<Rate> findByUserIdAndPostId(@Param("user") Long userId, @Param("post") Long postId);

}
