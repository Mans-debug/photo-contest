package com.itis.services;

import com.itis.dtos.*;
import com.itis.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PostService {
    PostDto createPost(AddPostDto addPostDto, Long userId);

    PostDto getById(Long postId);

    PostDto createForAlbum(AddPostDto addPostDto, Long albumId, Long userId);

    PostDto update(PostDto postDto, Long postId, Long userId);

    void delete(Long postId, Long userId);

    PostsPage getPostsByUser(Long userId, Integer page);

    List<PostDto> getTopPostsByUser(Long userId, Integer pageNumber);
}
