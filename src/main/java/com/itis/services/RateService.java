package com.itis.services;

import com.itis.dtos.AddRateDto;
import com.itis.dtos.RateDto;

public interface RateService {
    void createRate(Long userId, AddRateDto addRateDto);
}
