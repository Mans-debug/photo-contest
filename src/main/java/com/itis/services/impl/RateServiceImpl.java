package com.itis.services.impl;

import com.itis.dtos.AddRateDto;
import com.itis.dtos.DtoMapper;
import com.itis.dtos.RateDto;
import com.itis.exceptions.NoEntityFoundException;
import com.itis.exceptions.PostNotFoundException;
import com.itis.models.Post;
import com.itis.models.Rate;
import com.itis.models.User;
import com.itis.repositories.CommentRepository;
import com.itis.repositories.PostRepository;
import com.itis.repositories.RateRepository;
import com.itis.repositories.UserRepository;
import com.itis.services.RateService;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RateServiceImpl implements RateService {
    private final DtoMapper dtoMapper;
    private final RateRepository rateRepository;
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    @Override
    public void createRate(Long userId, AddRateDto addRateDto) {
        rateRepository.findByUserIdAndPostId(userId, addRateDto.getPostId())
                .ifPresentOrElse(r -> {
                    try {
                        r.setValue(addRateDto.getValue());
                        rateRepository.save(r);
                    } catch (Exception e) {
                        throw new NoEntityFoundException("Could find user or post");
                    }
                }, () ->{
                    Post post = postRepository.findById(addRateDto.getPostId()).orElseThrow(() -> new PostNotFoundException(addRateDto.getPostId(), "Could find post with id"));
                    User user = userRepository.getById(userId);
                    Rate rate = Rate.builder().post(post).value(addRateDto.getValue()).user(user).build();
                    rateRepository.save(rate);
                });
    }
}
