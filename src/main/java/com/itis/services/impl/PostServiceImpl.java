package com.itis.services.impl;

import com.itis.dtos.*;
import com.itis.exceptions.AlbumNotFoundException;
import com.itis.exceptions.IllegalActionException;
import com.itis.exceptions.PostNotFoundException;
import com.itis.models.Album;
import com.itis.models.Post;
import com.itis.models.User;
import com.itis.repositories.AlbumRepository;
import com.itis.repositories.PostRepository;
import com.itis.repositories.UserRepository;
import com.itis.services.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final UserRepository userRepository;
    private final AlbumRepository albumRepository;
    private final DtoMapper dtoMapper;

    private Integer defaultPageSize;

    @Value("${default-page-size}")
    public void setDefaultPageSize(Integer defaultPageSize) {
        this.defaultPageSize = defaultPageSize;
    }

    @Override
    public PostDto createPost(AddPostDto addPostDto, Long userId) {
        User user = userRepository.getById(userId);
        Post post;
        userRepository.save(user.addPost(
                post = dtoMapper.addPostToPost(addPostDto)
        ));
        return dtoMapper.postToPostDto(post);
    }

    @Override
    public PostDto getById(Long postId) {
        return dtoMapper.postToPostDto(
                postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(postId, "Could find post with id"))
        );
    }

    @Override
    public PostDto createForAlbum(AddPostDto addPostDto, Long albumId, Long userId) {
        User user = userRepository.getById(userId);
        Album album = albumRepository.findById(albumId).orElseThrow(() -> new AlbumNotFoundException(albumId, "Could find album with id"));
        Post post = dtoMapper.addPostToPost(addPostDto);
        post.setUser(user);
        album.getPosts().add(post);
        albumRepository.save(
            album
        );
        return dtoMapper.postToPostDto(post);
    }

    @Override
    public PostDto update(PostDto postDto, Long postId, Long userId) throws EntityNotFoundException, IllegalActionException {
        Post post = postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(postId, "Could find post with id"));
        if (!post.getUser().getId().equals(userId))
            throw new IllegalActionException();
        return dtoMapper.postToPostDto(
                postRepository.save(
                        dtoMapper.updatePost(post, postDto)
                )
        );
    }

    @Override
    public void delete(Long postId, Long userId) throws EntityNotFoundException, IllegalActionException {
        Post post = postRepository.findById(postId).orElseThrow(() -> new PostNotFoundException(postId, "Could find post with id"));
        if (!post.getUser().getId().equals(userId))
            throw new IllegalActionException();
        postRepository.delete(post);
    }

    @Override
    public PostsPage getPostsByUser(Long userId, Integer pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, defaultPageSize);
        Page<Post> page = postRepository.findAllByUser(userRepository.getById(userId), pageRequest);
        return PostsPage.builder()
                .posts(dtoMapper.postsToPostsDto(page.getContent()))
                .totalPages(page.getTotalPages())
                .build();
    }

    @Override
    public List<PostDto> getTopPostsByUser(Long userId, Integer pageNumber) {
        List<Post> posts = postRepository.userTop(userRepository.getById(userId));
        return dtoMapper.postsToPostsDto(posts);
    }
}
