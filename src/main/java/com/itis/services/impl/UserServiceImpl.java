package com.itis.services.impl;

import com.itis.dtos.DtoMapper;
import com.itis.dtos.UserDto;
import com.itis.exceptions.UserNotFoundException;
import com.itis.models.User;
import com.itis.repositories.FileInfoRepository;
import com.itis.repositories.UserRepository;
import com.itis.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final DtoMapper dtoMapper;
    private final FileInfoRepository fileInfoRepository;

    @Override
    public UserDto get(Long userId) throws EntityNotFoundException {
        return dtoMapper.userToUserDto(
                userRepository.findById(userId)
                        .orElseThrow(() ->new UserNotFoundException(userId, "Could not find user with id"))
        );
    }

    @Override
    public UserDto update(Long userId, UserDto userDto) {
        User user = userRepository.getById(userId);
        user.setProfilePicture(null);
        userRepository.save(user);
        user = dtoMapper.updateUser(
                userRepository.getById(userId), userDto);
        return dtoMapper.userToUserDto(
                userRepository.save(user));
    }
}
