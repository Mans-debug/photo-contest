package com.itis.services.impl;

import com.itis.dtos.AddAlbumDto;
import com.itis.dtos.AlbumDto;
import com.itis.dtos.DtoMapper;
import com.itis.exceptions.AlbumNotFoundException;
import com.itis.models.Album;
import com.itis.repositories.AlbumRepository;
import com.itis.services.AlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AlbumServiceImpl implements AlbumService {
    private final AlbumRepository albumRepository;
    private final DtoMapper dtoMapper;

    @Override
    public AlbumDto createAlbum(AddAlbumDto addAlbumDto) {
        return dtoMapper.albumToAlbumDto(
                albumRepository.save(
                        dtoMapper.addAlbumDtoToAlbum(addAlbumDto)
                )
        );
    }

    @Override
    public AlbumDto update(AlbumDto newAlbum, Long albumId) {
        return null;
    }

    @Override
    public List<AlbumDto> getAll() {
        return dtoMapper.albumListToAlbumDtoList(
                albumRepository.findAll()
        );
    }

    @Override
    public AlbumDto getById(Long albumId) throws EntityNotFoundException {
        return dtoMapper.albumToAlbumDto(
                albumRepository.findById(albumId)
                        .orElseThrow(() ->
                                new AlbumNotFoundException(albumId, "Could not find album with id"))
        );
    }
}
