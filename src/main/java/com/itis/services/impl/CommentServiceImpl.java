package com.itis.services.impl;

import com.itis.dtos.AddCommentDto;
import com.itis.dtos.CommentDto;
import com.itis.dtos.DtoMapper;
import com.itis.models.Comment;
import com.itis.models.Post;
import com.itis.models.User;
import com.itis.repositories.CommentRepository;
import com.itis.repositories.PostRepository;
import com.itis.repositories.UserRepository;
import com.itis.services.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.itis.exceptions.IllegalAccessException;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CommentServiceImpl implements CommentService {
    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final DtoMapper dtoMapper;

    @Override
    public CommentDto addComment(Long userId, AddCommentDto addCommentDto) {
        Post post = postRepository.findById(addCommentDto.getPostId())
                .orElseThrow(()->new IllegalAccessException("This comment doesn't belong to the user"));
        Comment createdComment = Comment.builder()
                .post(post)
                .user(userRepository.getById(userId))
                .timeCommented(LocalDateTime.now())
                .text(addCommentDto.getText())
                .build();
        commentRepository.save(createdComment);
        return dtoMapper.commentToCommentDto(createdComment);
    }

    @Override
    public CommentDto findById(Long commentId) {
        return dtoMapper.commentToCommentDto(commentRepository.findById(commentId));
    }
}
