package com.itis.services.impl;

import com.itis.dtos.DtoMapper;
import com.itis.dtos.FileInfoDto;
import com.itis.exceptions.NullFileException;
import com.itis.models.FileInfo;
import com.itis.repositories.FileInfoRepository;
import com.itis.services.FilesService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FilesServiceImpl implements FilesService {

    private final FileInfoRepository fileInfoRepository;
    private final DtoMapper dtoMapper;

    @Value("${files.storage.path}")
    private String path;

    @Override
    @Transactional
    public List<FileInfoDto> saveFiles(MultipartFile... files) {
        return Arrays.stream(files).map(this::saveFile).collect(Collectors.toList());
    }

    @Override
    public FileInfoDto saveFile(MultipartFile file) throws IllegalArgumentException{
        if (file.getOriginalFilename().isEmpty())
            throw new NullFileException("Uploaded file is null");
        FileInfo fileInfo = FileInfo.fromMultipart(file);
        String storageName = UUID.randomUUID() + getExtension(file);
        fileInfo.setStorageName(storageName);
        try {
            Files.copy(file.getInputStream(), Path.of(path + "\\" + storageName));
        } catch (IOException e) {
            throw new IllegalArgumentException("Problem with copying file to web storage");
        }
        return dtoMapper.fileInfoToFileInfoDto(
                fileInfoRepository.save(fileInfo)
        );
    }

    private String getExtension(MultipartFile file) {
        String originalName = file.getOriginalFilename();
        int lastDotIndex = originalName.lastIndexOf(".");
        return originalName.substring(lastDotIndex);
    }

    @Override
    public void downloadFile(Long id, OutputStream outputStream) {
        File file = new File(path + "\\" +
                fileInfoRepository.getById(id).getStorageName());

        try {
            Files.copy(file.toPath(), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException("Problem with copying file to web storage");
        }
    }

    @Override
    public void downloadFile(String uid, OutputStream outputStream) {
        File file = new File(path + "\\" + uid);

        try {
            Files.copy(file.toPath(), outputStream);
        } catch (IOException e) {
            throw new IllegalArgumentException("Problem with copying file to web storage");
        }
    }

}
