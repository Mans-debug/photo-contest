package com.itis.services;

import com.itis.dtos.FileInfoDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.OutputStream;
import java.util.List;

public interface FilesService {
    List<FileInfoDto> saveFiles(MultipartFile... files);

    FileInfoDto saveFile(MultipartFile files);

    void downloadFile(Long id, OutputStream outputStream);

    void downloadFile(String uid, OutputStream outputStream);
}
