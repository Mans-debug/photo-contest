package com.itis.services;

import com.itis.dtos.UserDto;
import com.itis.models.User;

public interface UserService {
    UserDto get(Long userId);

    UserDto update(Long userId, UserDto userDto);

}
