package com.itis.services;

import com.itis.dtos.AddCommentDto;
import com.itis.dtos.CommentDto;

public interface CommentService {
    CommentDto addComment(Long userId, AddCommentDto addCommentDto);

    CommentDto findById(Long commentId);
}
