package com.itis.services;

import com.itis.dtos.SignInDto;

public interface SignInService {
    void SignIn(SignInDto signInDto);
}
