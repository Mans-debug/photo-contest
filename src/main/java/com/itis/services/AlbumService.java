package com.itis.services;


import com.itis.dtos.AddAlbumDto;
import com.itis.dtos.AlbumDto;
import com.itis.models.Album;

import java.util.List;

public interface AlbumService {
    AlbumDto createAlbum(AddAlbumDto addAlbumDto);
    AlbumDto update(AlbumDto newAlbum, Long albumId);
    List<AlbumDto> getAll();
    AlbumDto getById(Long albumId);
}
