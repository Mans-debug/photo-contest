package com.itis.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Post {

    public Post(Long id, String title, String description){
        this.id = id;
        this.title = title;
        this.description = description;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private String title;

    private String description;

    @ManyToMany(mappedBy = "posts")
    @JsonIgnore
    private List<Album> albums;

    @OneToMany(mappedBy = "post")
    private List<Comment> comments;

    @OneToMany
    private List<FileInfo> photos;

    @OneToMany(mappedBy = "post")
    private List<Rate> rate;
}
