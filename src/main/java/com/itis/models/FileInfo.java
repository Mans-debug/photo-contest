package com.itis.models;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class FileInfo {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String originalName;

    private String storageName;

    private Long fileSize;

    private String contentType;

    //attention убрал из связи OneToMany клиентов. Зачем они?

    public static FileInfo fromMultipart(MultipartFile file){
        return FileInfo.builder()
                .originalName(file.getOriginalFilename())
                .fileSize(file.getSize())
                .contentType(file.getContentType())
                .build();
    }
}
