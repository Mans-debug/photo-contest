package com.itis.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String login;

    private String email;

    private String firstName;

    private String lastName;

    private String hashedPassword;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    private List<Post> posts;

    public enum Role {
        ADMIN, USER
    }

    @ManyToOne
    @JoinColumn(name = "file_id")
    private FileInfo profilePicture;

    public User addPost(Post post){
        if(posts == null)
            posts = new ArrayList<>();
        posts.add(post);
        post.setUser(this);
        return this;
    }
}
