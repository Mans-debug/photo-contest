package com.itis.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class IllegalAccessException extends RuntimeException{
    public IllegalAccessException(String message) {
        super(message);
        log.error("Someone tried to access resources, which don't belong to them");
    }
}
