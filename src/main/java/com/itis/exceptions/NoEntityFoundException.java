package com.itis.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@AllArgsConstructor
@Slf4j
public class NoEntityFoundException extends RuntimeException{
    private Object object;

    public NoEntityFoundException() {
        log.error("Could not find entity" + object.toString());
    }

    public NoEntityFoundException(String message) {
        super(message);
        log.error("Could not find entity" + message);
    }
    public NoEntityFoundException(Object obj, String message){
        super(message);
        object = obj;
        log.error("Could not find entity" + message + "\n" + object.toString());

    }
}
