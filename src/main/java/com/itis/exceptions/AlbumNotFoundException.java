package com.itis.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AlbumNotFoundException extends NoEntityFoundException {
    public AlbumNotFoundException(Long albumId, String s) {
        super(albumId, s);
        log.error("Could not find album");
    }
}
