package com.itis.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NullFileException extends NoEntityFoundException{
    public NullFileException(String message) {
        super(message);
        log.error("Null file was uploaded");
    }
}
