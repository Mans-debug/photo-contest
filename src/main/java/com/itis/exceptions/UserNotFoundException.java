package com.itis.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserNotFoundException extends NoEntityFoundException {
    public UserNotFoundException(Object obj, String s) {
        super(obj, s);
        log.error("Someone tried to access non-existing user");
    }
}
