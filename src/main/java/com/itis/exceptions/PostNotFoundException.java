package com.itis.exceptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PostNotFoundException extends NoEntityFoundException {
    public PostNotFoundException(Object obj, String message) {
        super(obj, message);
        log.error("Someone tried to access non-existing post");
    }
}
